import 'package:vashisht/constants.dart';
import 'package:vashisht/models/auth.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class AuthService {
  static Future<RegisterResponse> register(RegisterRequest request) {
    var url = "${AppConstants.baseURL}/users";
    return http.post(url, body: json.encode(request.toJson()))
        .then((response) => json.decode(response.body))
        .then(((map) => RegisterResponse.fromJson(map)));
  }

  static Future<RegisterResponse> login(LoginRequest request) {
    var url = "${AppConstants.baseURL}/users/login";
    return http.post(url, body: json.encode(request.toJson()))
        .then((response) => json.decode(response.body))
        .then(((map) => RegisterResponse.fromJson(map)));
  }
}
