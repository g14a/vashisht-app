import 'package:vashisht/constants.dart';
import 'package:vashisht/models/event.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:vashisht/models/event_registration.dart';

class EventService {
  static Future<List<Event>> getEvents({bool mock}) async {
    if (!mock) {
      var url = "${AppConstants.baseURL}/events";
      var eventsFuture = http.get(url)
          .then((response) => json.decode(response.body))
          .then((maps) => maps as List<dynamic>)
          .then((mapList) => mapList.map((map) => Event.fromJson(map)).toList());
      return Future.value(eventsFuture);
    } else {
      var events = [
        Event(name: "Code obfuscation", category: "Quiz", fee: 50, didRegister: false),
        Event(name: "Tech talk", category: "Talk", fee: 0, didRegister: false),
        Event(name: "Tech talk", category: "Talk", fee: 0, didRegister: true)
      ];
      return Future.delayed(Duration(seconds: 1), () => events);
    }
  }

  static Future<EventRegistrationResponse> register(int userid, int eventid) {
    var baseURL = AppConstants.baseURL;
    var path = "/users/$userid/events/$eventid/register";
    return http.post(baseURL + path)
    .then((response) => json.decode(response.body))
    .then((map) => EventRegistrationResponse.fromJson(map));
  }

  static Future<String> checkRegistration(int userid, int eventid) {
    if (userid == null) {
      return Future.error(AppConstants.invalidLogin);
    }
    var baseURL = AppConstants.baseURL;
    var path = "/users/$userid/events/$eventid/check";
    return http.get(baseURL + path)
        .then((response) => response.body);
  }

  static Future<String> cancelRegistration(int userid, int eventid) {
    var baseURL = AppConstants.baseURL;
    var path = "/users/$userid/events/$eventid/cancel";
    return http.delete(baseURL + path)
        .then((response) => response.body);
  }
}