import 'package:flutter/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vashisht/api/event_service.dart';
import 'package:vashisht/builders/authenticated_user_builder.dart';
import 'package:vashisht/models/event.dart';
import 'package:flutter/material.dart';
import 'package:share/share.dart';
import 'package:vashisht/pages/login_page.dart';

class EventActionsWidget extends StatefulWidget {
  final Event event;

  EventActionsWidget({this.event});

  @override
  State<StatefulWidget> createState() => EventsActionState(event: event);
}

class EventsActionState extends State<EventActionsWidget> {
  final Event event;

  EventsActionState({this.event});

  @override
  Widget build(BuildContext context) {
    return AuthenticatedUserBuilder(
        onSuccess: (ctx, snapshot) {
          var userid = snapshot.data.getInt("userid");
          var checkRegistrationResponse =
              EventService.checkRegistration(userid, event.id);

          return FutureBuilder<String>(
            future: checkRegistrationResponse,
            builder: (BuildContext ctx, AsyncSnapshot<String> snapshot) {
              var didRegister = false;
              Widget primaryAction;

              if (snapshot.hasData) {
                if (snapshot.data != null) {
                  didRegister = snapshot.data == "false" ? false : true;

                  var actionText = didRegister
                      ? "CANCEL REGISTRATION"
                      : ((event.fee == 0 || event.fee == null)
                          ? "REGISTER FOR FREE"
                          : "PAY Rs. ${event.fee}");
                  var actionColor =
                      didRegister ? Colors.amber : Colors.lightBlueAccent;
                  primaryAction = FlatButton(
                      onPressed: didRegister ? deregister : register,
                      child: Text(actionText),
                      textColor: actionColor);
                }
              } else {
                primaryAction = FlatButton(
                    onPressed: () => Navigator.push(context,
                        MaterialPageRoute(builder: (ctx) => LoginPage())),
                    child: Text(
                      "Login to register",
                      style: TextStyle(color: Colors.amberAccent),
                    ));
              }

              return Container(
                padding: EdgeInsets.only(left: 4.0, right: 4.0),
                child: Row(
                  children: <Widget>[
                    IconButton(
                      icon: Icon(
                        Icons.share,
                        color: Colors.white70,
                      ),
                      onPressed: share,
                      splashColor: Colors.green,
                    ),
                    Spacer(),
                    primaryAction
                  ],
                ),
              );
            },
          );
        },
        onFailure: (ctx) {

          Widget primaryAction = FlatButton(
              onPressed: () => Navigator.push(context,
                  MaterialPageRoute(builder: (ctx) => LoginPage())),
              child: Text(
                "Login to register",
                style: TextStyle(color: Colors.amberAccent),
              ));

          return Container(
            padding: EdgeInsets.only(left: 4.0, right: 4.0),
            child: Row(
              children: <Widget>[
                IconButton(
                  icon: Icon(
                    Icons.share,
                    color: Colors.white70,
                  ),
                  onPressed: share,
                  splashColor: Colors.green,
                ),
                Spacer(),
                primaryAction
              ],
            ),
          );
        });
  }

  share() {
    try {
      Share.share("I'm attending ${event.name} at Samgatha 2019");
    } catch (ex) {
      print("Exception while executing share: ${ex.toString()}");
    }
  }

  register() async {
    var preferences = SharedPreferences.getInstance();

    preferences.then((prefs) {
      var userid = prefs.getInt("userid");

      if (userid == null) {}

      if (event.fee > 0) {
        payToRegister();
      }

      EventService.register(userid, event.id).then((response) {
        setState(() {
          var snackBar = SnackBar(
            content: Text("Registered for ${event.name}"),
            action: SnackBarAction(label: "UNDO", onPressed: () {}),
          );
          Scaffold.of(context).showSnackBar(snackBar);
        });
      });
    });
  }

  payToRegister() {}

  deregister() {
    var preferences = SharedPreferences.getInstance();

    preferences.then((prefs) {
      var userid = prefs.getInt("userid");
      EventService.cancelRegistration(userid, event.id).then((response) {
        setState(() {});
      }).catchError((err) {});
    });
  }
}
