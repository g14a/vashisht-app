import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vashisht/models/event.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:vashisht/pages/event_page.dart';
import 'package:vashisht/widgets/event_actions.dart';

class EventWidget extends StatelessWidget {
  final Event event;

  EventWidget({this.event});

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;

    return GestureDetector(
      onTap: () => Navigator.push(
          context, MaterialPageRoute(builder: (context) => EventPage(event: event,))),
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4.0)),
        clipBehavior: Clip.antiAlias,
        margin: EdgeInsets.only(top: 16.0, left: 8.0, right: 8.0),
        child: Container(
          child: Stack(
            children: <Widget>[
              Container(
                child: FadeInImage.memoryNetwork(
                  height: width * 9 / 16,
                  width: width,
                  fadeInCurve: Curves.easeIn,
                  fit: BoxFit.fitWidth,
                  placeholder: kTransparentImage,
                  image:
                      'https://images.pexels.com/photos/414171/pexels-photo-414171.jpeg?cs=srgb&dl=adventure-calm-clouds-414171.jpg&fm=jpg',
                ),
              ),
              renderBlackScrim(width),
              Positioned(
                bottom: 0.0,
                left: 0.0,
                right: 0.0,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(16.0),
                      child: Row(
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                event.name,
                                style: TextStyle(
                                    fontSize: 18.0, color: Colors.white),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 8.0),
                                child: Text(
                                  event.category,
                                  style: TextStyle(
                                      fontSize: 14.0,
                                      color: Colors.white70,
                                      fontWeight: FontWeight.w600),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                    EventActionsWidget(
                      event: event
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget renderBlackScrim(double width) => Container(
        width: width,
        height: width * 9 / 16,
        decoration: BoxDecoration(
          // Box decoration takes a gradient
          gradient: LinearGradient(
            // Where the linear gradient begins and ends
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            // Add one stop for each color. Stops should increase from 0 to 1
            stops: [0.1, 0.5, 0.7, 0.9],
            colors: [
              // Colors are easy thanks to Flutter's Colors class.
              Color.fromRGBO(0, 0, 0, 0.1),
              Color.fromRGBO(0, 0, 0, 0.3),
              Color.fromRGBO(0, 0, 0, 0.4),
              Color.fromRGBO(0, 0, 0, 0.5)
            ],
          ),
        ),
      );
}
