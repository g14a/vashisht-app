import 'package:flutter/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';

typedef AuthenticatedBuilder = Widget Function(
    BuildContext, AsyncSnapshot<SharedPreferences>);
typedef UnauthenticatedBuilder = Widget Function(BuildContext);

FutureBuilder<SharedPreferences> AuthenticatedUserBuilder(
    {@required AuthenticatedBuilder onSuccess,
    @required UnauthenticatedBuilder onFailure}) {
  var prefsFuture = SharedPreferences.getInstance();

  return FutureBuilder<SharedPreferences>(
    future: prefsFuture,
    builder: (BuildContext ctx, AsyncSnapshot<SharedPreferences> snapshot) {
      if (snapshot.hasData) {
        if (snapshot.data != null) {
          var isLoggedIn = snapshot.data.getBool("isLoggedIn");
          if (isLoggedIn != null) {
            if (isLoggedIn) {
              return onSuccess(ctx, snapshot);
            }
          }
        }
      }
      return onFailure(ctx);
    },
  );
}
