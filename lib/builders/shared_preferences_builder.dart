import 'package:flutter/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';

typedef SnapshotBuilder<T> = Widget Function(BuildContext, AsyncSnapshot<T>);

FutureBuilder<SharedPreferences> SharedPreferencesBuilder(
    {@required SnapshotBuilder<SharedPreferences> onDone}) {
  var prefsFuture = SharedPreferences.getInstance();

  return FutureBuilder<SharedPreferences>(
    future: prefsFuture,
    builder: (BuildContext ctx, AsyncSnapshot<SharedPreferences> snapshot) {
      return onDone(ctx, snapshot);
    },
  );
}
