class EventRegistrationResponse {
  int eventid;
  int userid;
  String regid;

  EventRegistrationResponse.fromJson(dynamic json) {
   var jsonString = json as Map<String, dynamic>;
   eventid = jsonString["eventid"];
   userid = jsonString["userid"];
   regid = jsonString["regid"];
  }
}