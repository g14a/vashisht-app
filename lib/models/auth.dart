class RegisterRequest {
  String name;
  String email;
  String phone;
  String passwordHash;
  String college;

  RegisterRequest({this.name, this.email, this.phone, this.passwordHash, this.college});

  Map<String, String> toJson() {
    var map = Map<String, String>();
    map["name"] = name;
    map["pwd"] = passwordHash;
    map["college"] = college;
    map["email"] = email;
    map["number"] = phone;
    return map;
  }
}

class RegisterResponse {
  String name;
  String email;
  String phone;
  String passwordHash;
  String college;
  int id;

  RegisterResponse({this.name, this.email, this.phone, this.passwordHash, this.college, this.id});

  RegisterResponse.fromJson(dynamic json) {
    var jsonString = json as Map<String, dynamic>;
    name = jsonString["name"];
    email = jsonString["email"];
    phone = jsonString["number"];
    college = jsonString["college"];
    id = jsonString["userid"];
  }
}

class LoginRequest {
  String email;
  String passwordHash;

  LoginRequest({this.email, this.passwordHash});

  Map<String, String> toJson() {
    var map = Map<String, String>();
    map["pwd"] = passwordHash;
    map["email"] = email;
    print(map);
    return map;
  }
}