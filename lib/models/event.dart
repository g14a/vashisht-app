class Event {
  int id;
  String name;
  String category;
  bool didRegister;
  int day;
  int fee;
  int teamSize;
  String startTime;
  String endTime;

  Event({this.id, this.name, this.category, this.didRegister, this.day, this.startTime, this.endTime, this.fee, this.teamSize});

  Event.fromJson(dynamic json) {
    var jsonString = json as Map<String, dynamic>;
    id = jsonString["id"];
    name = jsonString["name"];
    category = jsonString["category"];
    fee = jsonString["fee"];
    didRegister = false;
  }
}