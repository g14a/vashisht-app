class AppConstants {
  static final baseURL = "https://quiet-reef-46852.herokuapp.com";
  //static final baseURL = "http://192.168.1.5:8000";

  static final invalidLogin = "INVALID_LOGIN";
}