import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:vashisht/api/event_service.dart';
import 'package:vashisht/builders/authenticated_user_builder.dart';
import 'package:vashisht/builders/shared_preferences_builder.dart';
import 'package:vashisht/models/event.dart';
import 'package:vashisht/widgets/event_widget.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vashisht/pages/login_page.dart';
import 'package:vashisht/pages/qr_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Events'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  @override
  Widget build(BuildContext context) {

    var drawerHeaderFutureBuilder =
        SharedPreferencesBuilder(onDone: (ctx, snapshot) {
      if (!snapshot.hasData) {
        return ListTile(
          leading: Icon(Icons.account_circle),
          title: Text("Sign in"),
          onTap: () => Navigator.push(
              ctx, MaterialPageRoute(builder: (context) => LoginPage())),
        );
      } else {
        if (snapshot.data != null) {
          if (snapshot.data.getBool("isLoggedIn") == null) {
            return Container(
              child: ListTile(
                leading: Icon(Icons.account_circle),
                title: Text(
                  "Sign in",
                  style: TextStyle(color: Colors.blue),
                ),
                onTap: () => Navigator.push(
                    ctx, MaterialPageRoute(builder: (context) => LoginPage())),
              ),
            );
          }
          if (snapshot.data.getBool("isLoggedIn")) {
            return UserAccountsDrawerHeader(
              accountName: Text(
                snapshot.data.getString("name"),
                style: TextStyle(color: Colors.black87),
              ),
              accountEmail: Text(
                snapshot.data.getString("email"),
                style: TextStyle(
                    color: Colors.black54, fontWeight: FontWeight.w600),
              ),
              decoration: BoxDecoration(color: Colors.transparent),
              currentAccountPicture: Container(
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(35),
                  child: FadeInImage(
                    fit: BoxFit.cover,
                    placeholder: AssetImage(
                      'food/cherry_pie.png',
                      package: 'flutter_gallery_assets',
                    ),
                    image: NetworkImage(
                        'https://images.pexels.com/photos/414171/pexels-photo-414171.jpeg?cs=srgb&dl=adventure-calm-clouds-414171.jpg&fm=jpg'),
                  ),
                ),
              ),
            );
          } else {
            return Container(
              child: ListTile(
                leading: Icon(Icons.account_circle),
                title: Text(
                  "Sign in",
                  style: TextStyle(color: Colors.blue),
                ),
                onTap: () => Navigator.push(
                    ctx, MaterialPageRoute(builder: (context) => LoginPage())),
              ),
            );
          }
        } else {
          return Container();
        }
      }
    });

    return Scaffold(
        drawer: getDrawer(drawerHeaderFutureBuilder),
        backgroundColor: Colors.white,
        body: RefreshIndicator(
            child: FutureBuilder<List<Event>>(
                future: EventService.getEvents(mock: false),
                builder: (BuildContext ctx,
                    AsyncSnapshot<List<Event>> eventsSnapshot) {
                  if (!eventsSnapshot.hasData) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  } else {
                    if (eventsSnapshot.data != null) {
                      return ListView(
                          children: eventsSnapshot.data
                              .map((event) => EventWidget(
                                    event: event,
                                  ))
                              .toList());
                    }
                  }
                }),
            onRefresh: () {
              setState(() {});
              return EventService.getEvents(mock: false);
            }));
  }

  Widget renderBottomAppbar(BuildContext ctx) {
    return BottomAppBar(
      color: Color.fromRGBO(40, 40, 40, 1),
      child: Padding(
        padding: EdgeInsets.only(left: 16.0, right: 16.0),
        child: Row(
          children: <Widget>[
            Builder(builder: (BuildContext ctx) {
              return IconButton(
                  icon: Icon(
                    Icons.menu,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    Scaffold.of(ctx).openDrawer();
                  });
            }),
            Spacer(flex: 2),
            Text(
              "Events",
              style: TextStyle(color: Colors.white, fontSize: 20.0),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }

  Widget getDrawer(FutureBuilder<SharedPreferences> drawerHeaderFutureBuilder) {
    var screenWidth = MediaQuery.of(context).size.width;

    var prefsFuture = SharedPreferences.getInstance();

    return Drawer(
      child: ListView(
        children: <Widget>[
          drawerHeaderFutureBuilder,
          ListTile(
            title: Text("Events"),
            leading: Icon(Icons.event),
            onTap: () {},
          ),
          AuthenticatedUserBuilder(
              onSuccess: (ctx, snapshot) => ListTile(
                    title: Text("My Calendar"),
                    leading: Icon(Icons.calendar_today),
                    onTap: () {},
                  ),
              onFailure: (ctx) => Container()),
          AuthenticatedUserBuilder(
              onSuccess: (ctx, snapshot) => ListTile(
                    title: Text("MyQR"),
                    leading: Icon(Icons.center_focus_strong),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext ctx) => QrPage()));
                    },
                  ),
              onFailure: (ctx) => Container()),
          AuthenticatedUserBuilder(
              onSuccess: (ctx, snapshot) => ListTile(
                    title: Text(
                      "Logout",
                      style: TextStyle(color: Colors.red),
                    ),
                    leading: Icon(Icons.exit_to_app),
                    onTap: () async {
                      var prefsFuture = await SharedPreferences.getInstance();
                      prefsFuture.setBool("isLoggedIn", false);
                      prefsFuture.setInt("userid", null);
                      prefsFuture.setString("name", null);
                      prefsFuture.setString("email", null);
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => MyHomePage(
                                    title: "Events",
                                  )));
                    },
                  ),
              onFailure: (ctx) => Container()),
          Divider(),
          ListTile(
            title: Text("Settings"),
            leading: Icon(Icons.settings),
            onTap: () {},
          ),
          FutureBuilder<SharedPreferences>(
            future: prefsFuture,
            builder:
                (BuildContext ctx, AsyncSnapshot<SharedPreferences> snapshot) {
              if (snapshot.hasData) {
                if (snapshot.data != null) {
                  var userid = snapshot.data.getInt("userid");
                  if (userid != null) {
                    return Container(
                      padding: EdgeInsets.only(top: 32.0, bottom: 32.0),
                      child: Center(
                          child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            width: 2 * screenWidth / 5,
                            child: Text(
                              "Scan this code to enter into registered events",
                              style: TextStyle(fontSize: 14.0),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 16.0),
                            child: QrImage(
                                data: userid.toString(),
                                size: 2 * screenWidth / 5),
                          )
                        ],
                      )),
                    );
                  } else {
                    return Container();
                  }
                }
              }
              return Container();
            },
          )
        ],
      ),
    );
  }
}
