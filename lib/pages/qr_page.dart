import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class QrPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;

    var prefsFuture = SharedPreferences.getInstance();

    var qrBuilder = FutureBuilder<SharedPreferences>(
      future: prefsFuture,
      builder: (BuildContext ctx, AsyncSnapshot<SharedPreferences> snapshot) {
        if (snapshot.hasData) {
          if (snapshot.data != null) {
            var userid = snapshot.data.getInt("userid");
            return Container(
              child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        width: 0.75 * screenWidth,
                        child: Text(
                          "Scan this code to enter into registered events",
                          style: TextStyle(fontSize: 20.0),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 16.0),
                        child: QrImage(data: userid.toString(), size: 0.75 * screenWidth),
                      )
                    ],
                  )),
            );
          }
        }
        return CircularProgressIndicator();
      },
    );

    return Scaffold(
      bottomNavigationBar: BottomAppBar(
        color: Color.fromRGBO(40, 40, 40, 1),
        child: Padding(
          padding: EdgeInsets.only(left: 16.0, right: 16.0),
          child: Row(
            children: <Widget>[
              Builder(builder: (BuildContext ctx) {
                return IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      Navigator.pop(ctx);
                    });
              }),
              Spacer(),
            ],
          ),
        ),
      ),
      body: qrBuilder
    );
  }
}
