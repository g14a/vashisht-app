import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vashisht/api/auth_service.dart';
import 'package:vashisht/main.dart';
import 'package:vashisht/models/auth.dart';
import 'package:vashisht/pages/register_page.dart';

class LoginForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => LoginFormState();
}

class LoginFormState extends State<LoginForm> {
  final _loginFormKey = GlobalKey<FormState>();

  Widget submitButton;
  Widget registerRedirectButton;

  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var androidSubmitButton = RaisedButton(
      onPressed: _processLoginForm,
      child: Text("Submit", style: TextStyle(color: Colors.white, fontSize: 16.0),),
      padding: EdgeInsets.only(top: 16.0, bottom: 16.0),
      color: Colors.blue,
    );
    var iosSubmitButton = CupertinoButton(
      onPressed: _processLoginForm,
      color: CupertinoColors.activeBlue,
      child: Text("Submit"),
      pressedOpacity: 0.8,
    );
    var androidRegisterButton = RaisedButton(
      onPressed: _redirectToRegisterPage,
      child: Text("Register now"),
    );
    var iosRegisterButton = CupertinoButton(
      onPressed: _redirectToRegisterPage,
      child: Text(
        "Register now",
        style: TextStyle(color: CupertinoColors.black),
      ),
      color: CupertinoColors.lightBackgroundGray,
      pressedOpacity: 0.8,
    );

    if (Platform.isIOS) {
      submitButton = iosSubmitButton;
      registerRedirectButton = iosRegisterButton;
    } else if (Platform.isAndroid) {
      submitButton = androidSubmitButton;
      registerRedirectButton = androidRegisterButton;
    }

    return Padding(
      padding: EdgeInsets.all(16.0),
      child: Form(
        key: _loginFormKey,
        child: ListView(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 16.0, bottom: 16.0),
              child: TextFormField(
                controller: emailController,
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                    border: UnderlineInputBorder(), hintText: "Email address"),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Email address cannot be empty';
                  }
                },
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 16.0, bottom: 16.0),
              child: TextFormField(
                controller: passwordController,
                keyboardType: TextInputType.text,
                obscureText: true,
                decoration: InputDecoration(
                    border: UnderlineInputBorder(), hintText: "Password"),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Password cannot be empty';
                  }
                },
              ),
            ),
            submitButton,
            Padding(
              padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 64.0),
              child: Column(
                children: <Widget>[
                  Text("Don't have an account with us yet?"),
                  Padding(
                    padding: EdgeInsets.only(top: 16.0),
                    child: registerRedirectButton,
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _processLoginForm() {
    if (_loginFormKey.currentState.validate()) {
      callAuthServiceForLogin().then((loginResponse) async {
        if (loginResponse.id != null) {
          // Login is successful
          var prefsFuture = await SharedPreferences.getInstance();
          prefsFuture.setBool("isLoggedIn", true);
          prefsFuture.setInt("userid", loginResponse.id);
          prefsFuture.setString("name", loginResponse.name);
          prefsFuture.setString("email", loginResponse.email);
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => MyHomePage(
                        title: "Events",
                      )));
        }
      }).catchError((err) {});
    }
  }

  Future<RegisterResponse> callAuthServiceForLogin() {
    var email = emailController.text;
    var passwordHash = passwordController.text;

    var loginRequest = LoginRequest()
      ..email = email
      ..passwordHash = passwordHash;

    return AuthService.login(loginRequest);
  }

  void _redirectToRegisterPage() => Navigator.push(
      context, MaterialPageRoute(builder: (context) => RegisterPage()));
}
