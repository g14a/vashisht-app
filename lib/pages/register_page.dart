import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vashisht/pages/register_form.dart';

class RegisterPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Create your account"),
        ),
        body: RegisterForm()
    );
  }
}