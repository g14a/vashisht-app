import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vashisht/pages/login_form.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Sign in to your account"),
      ),
      body: LoginForm()
    );
  }
}