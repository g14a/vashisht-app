import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vashisht/models/event.dart';
import 'package:transparent_image/transparent_image.dart';

class EventPage extends StatelessWidget {

  final Event event;

  EventPage({this.event});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              expandedHeight: 200.0,
              floating: false,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                centerTitle: false,
                  title: Text(event.name,
                      textAlign: TextAlign.start,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18.0,
                      )),
                  background: FadeInImage.memoryNetwork(
                    fadeInCurve: Curves.easeIn,
                    fit: BoxFit.fitWidth,
                    placeholder: kTransparentImage,
                    image:
                    'https://images.pexels.com/photos/414171/pexels-photo-414171.jpeg?cs=srgb&dl=adventure-calm-clouds-414171.jpg&fm=jpg',
                  ),),
            ),
          ];
        },
        body: Center(
          child: Text("Sample Text"),
        ),
      ),
    );
  }
}