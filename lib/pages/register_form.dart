import 'dart:convert';
import 'dart:io';
import 'package:convert/convert.dart';
import 'package:crypto/crypto.dart' as crypto;

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vashisht/api/auth_service.dart';
import 'package:vashisht/main.dart';
import 'package:vashisht/models/auth.dart';

class RegisterForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => RegisterFormState();
}

class RegisterFormState extends State<RegisterForm> {
  final _registerFormKey = GlobalKey<FormState>();

  Widget submitButton;

  String passwordFieldValue;

  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController collegeController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var androidSubmitButton = RaisedButton(
      onPressed: _processRegisterForm,
      child: Text("Register"),
    );
    var iosSubmitButton = CupertinoButton(
      onPressed: _processRegisterForm,
      color: CupertinoColors.activeBlue,
      child: Text("Register"),
    );

    if (Platform.isIOS) {
      submitButton = iosSubmitButton;
    } else if (Platform.isAndroid) {
      submitButton = androidSubmitButton;
    }

    return Padding(
      padding: EdgeInsets.all(16.0),
      child: Form(
        key: _registerFormKey,
        child: ListView(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 16.0, bottom: 16.0),
              child: TextFormField(
                keyboardType: TextInputType.text,
                controller: nameController,
                decoration: InputDecoration(
                    border: UnderlineInputBorder(), hintText: "Name"),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Name cannot be empty';
                  }
                },
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 16.0, bottom: 16.0),
              child: TextFormField(
                keyboardType: TextInputType.emailAddress,
                controller: emailController,
                decoration: InputDecoration(
                    border: UnderlineInputBorder(), hintText: "Email address"),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Email address cannot be empty';
                  }
                },
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 16.0, bottom: 16.0),
              child: TextFormField(
                keyboardType: TextInputType.phone,
                controller: phoneController,
                decoration: InputDecoration(
                    border: UnderlineInputBorder(), hintText: "Mobile number"),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Mobile number cannot be empty';
                  }
                },
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 16.0, bottom: 16.0),
              child: TextFormField(
                keyboardType: TextInputType.text,
                controller: passwordController,
                obscureText: true,
                decoration: InputDecoration(
                    border: UnderlineInputBorder(), hintText: "Password"),
                validator: (value) {
                  passwordFieldValue = value;
                  if (value.isEmpty) {
                    return 'Password cannot be empty';
                  }
                },
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 16.0, bottom: 16.0),
              child: TextFormField(
                keyboardType: TextInputType.text,
                obscureText: true,
                decoration: InputDecoration(
                    border: UnderlineInputBorder(),
                    hintText: "Confirm password"),
                validator: (value) {
                  if (value != passwordFieldValue) {
                    return "Password don't match";
                  }
                },
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 16.0, bottom: 16.0),
              child: TextFormField(
                keyboardType: TextInputType.text,
                controller: collegeController,
                obscureText: true,
                decoration: InputDecoration(
                    border: UnderlineInputBorder(),
                    hintText: "College name"),
                validator: (value) {
                  if (value.isEmpty) {
                    return "College name cannot be empty";
                  }
                },
              ),
            ),
            submitButton
          ],
        ),
      ),
    );
  }

  void _processRegisterForm() {
    if (_registerFormKey.currentState.validate()) {
      callAuthServiceForRegister().then((registerResponse) async {
        if (registerResponse.id != null) {
          // Registration is successful
          var prefsFuture = await SharedPreferences.getInstance();
          prefsFuture.setBool("isLoggedIn", true);
          prefsFuture.setInt("userid", registerResponse.id);
          prefsFuture.setString("name", registerResponse.name);
          prefsFuture.setString("email", registerResponse.email);
          Navigator.push(context, MaterialPageRoute(builder: (context) => MyHomePage(title: "Events",)));
        }
      });
    }
  }

  Future<RegisterResponse> callAuthServiceForRegister() {
    var name = nameController.text;
    var passwordHash = _generateMd5(passwordController.text);
    var phone = phoneController.text;
    var college = collegeController.text;
    var email = emailController.text;

    var registerRequest = RegisterRequest()
      ..name = name
      ..passwordHash = passwordHash
      ..phone = phone
      ..college = college
      ..email = email;

    return AuthService.register(registerRequest);
  }

  ///Generate MD5 hash
  String _generateMd5(String data) {
    var content = Utf8Encoder().convert(data);
    var md5 = crypto.md5;
    var digest = md5.convert(content);
    return hex.encode(digest.bytes);
  }
}
